@echo off

if not exist obj md obj
if not exist output md output
cd obj
if not exist image md image
cd ..

call env.bat

set PROJECTNAME=10p_ota_service
set output_dir=.\output

del /f %output_dir%\%PROJECTNAME%_sign.bin

make -s %1 all

if %ERRORLEVEL% EQU 0 (
    mkimage sign rsa.private 1 0xA7260102 %output_dir%\%PROJECTNAME%.bin %output_dir%\%PROJECTNAME%_sign.bin
)

echo 111

copy /B /Y  %output_dir%\%PROJECTNAME%_sign.bin ..\build
