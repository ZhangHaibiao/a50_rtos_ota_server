#ifndef __LITEOS_API_
#define __LITEOS_API_

#ifndef u32
#define u32 unsigned int
#endif

#ifndef u16
#define u16 unsigned short
#endif

#ifndef u8
#define u8 unsigned char
#endif

#ifndef NULL
#define NULL    ((void *)0)
#endif

#undef  SEEK_CUR
#undef  SEEK_SET
#undef  SEEK_END
#define SEEK_CUR        0
#define SEEK_SET        1
#define SEEK_END        2

#define TX_CLEAR        (0x01)
#define RX_CLEAR        (0x02)
/* Device information */
#define MACH_NAME               "MACH_NAME"
#define PN_NAME                 "PN"
#define SN1_NAME                "SN1"
#define LOW_BAT_NAME            "LOW_BAT"
#define BAT_PERCENT             "BAT_PERCENT"
#define CHARG_NAME              "CHARGING"
#define LCD_W_NAME              "LCD_W"
#define LCD_H_NAME              "LCD_H"
#define SBI_VER_NAME            "SBI_VER"
#define LVOS_VER_NAME           "LVOS_VER"
#define HW_VER_NAME             "HW_VER"
#define TUSN                    "TUSN"
#define CELL_RSSI               "CELL_RSSI"
#define CELL_MCC                "CELL_MCC"
#define CELL_MNC                "CELL_MNC"
#define CELL_LAC                "CELL_LAC"
#define CELL_CI                 "CELL_CI"
#define CELL_SIG_LEV            "CELL_SIGNAL_LEVEL"

#define SIM_CID                 "SIM_CID"
#define DEV_IMEI                "IMEI"
#define DEV_IMSI                "IMSI"


#define     KEY1                0x31
#define     KEY2                0x32
#define     KEY3                0x33
#define     KEY4                0x34
#define     KEY5                0x35
#define     KEY6                0x36
#define     KEY7                0x37
#define     KEY8                0x38
#define     KEY9                0x39
#define     KEY0                0x30
#define     KEYFN               0x01
#define     KEYALPHA            0x02
#define     KEYCLEAR            0x03
#define     KEYUP               0x05
#define     KEYDOWN             0x06
#define     KEYENTER            0x0d
#define     KEYCANCEL           0x1b
#define     NOKEY               0xff

typedef struct{
	u32 starttime;
	u32 timeout;	
}ST_Timer_UNIT;

typedef struct{
    u16 year;
    u16 mon;
    u16 day;
    u16 hour;
    u16 min;
    u16 sec;
    u16 wday;
}SYS_TIME;

typedef struct{
    u8      Command[4];
    u16     Lc;
    u8      indata[256];
    u16     Le;
}C_APDU;

typedef struct{
    u16     outlen;
    u8      outdata[256];
    u8      SWA;
    u8      SWB;
}R_APDU; 

typedef  struct{
    u32     modlen;
    u8      mod[256];
    u8      exponent[4];
    u8      randlen;
    u8      random[8];
}ST_SCPINKEY; 

typedef  enum {
    SYS_POWER_SLEEP,
    SYS_POWER_REBOOT,
    SYS_POWER_SHUTDOWN,
    SYS_POWER_MAX
}SYS_POWER;

typedef  enum { 
	SEC_MKSK =0, 
	SEC_DUKPT, 
	SEC_RSA_KEY 
}SEC_KEY_TYPE;

typedef  enum { 
	KEY_FMT_NORMAL =0, 
	KEY_FMT_TR31 
}KEY_BLOCK_FMT;

typedef  enum { 
	DUKPT_KEY_PIN =0, 
	DUKPT_KEY_MAC_BOTH, 
	DUKPT_KEY_MAC_RSP, 
	DUKPT_KEY_DATA_BOTH,
	DUKPT_KEY_DATA_RSP 
}SEC_DUKPT_KEY_SELECT;

typedef  enum {
	SEC_CTRL_GETKCV=0,
	SEC_CTRL_DUKPT_ADD_KSN,
	SEC_CTRL_DUKPT_GET_KSN
}EM_SEC_CTRL_CODE;

typedef struct{
    u16  key_usage;
    u8   key_alg;
}ST_KEY_Attr;

typedef  enum {
	SEC_KU_DUKPT_INIT_KEY,
	SEC_KU_MASTER_KEY,
	SEC_KU_KBPK,
	SEC_KU_PIN_KEY,
	SEC_KU_DATA_KEY,
	SEC_KU_MAC_KEY
}key_usage;

typedef  enum {
	SEC_KA_AES,
	SEC_KA_TDEA
}key_alg;

typedef struct{
	u16 keylen;
	u8 *keydata;
	u16	kcvmode;
	u16 kcvlen;
	u8 	*kcv;
	u8  *iksn;
}ST_API_FMT_NORMAL_KEY_Block;

typedef  enum {
	KCV_MODE_NOCHK,
	KCV_MODE_CHK0,
	KCV_MODE_CHKFIX,
	KCV_MODE_CHKMAC
}kcvmode;

typedef struct{
	u32 version;
	u16 depend_type;
	u16 depend_id;
	u16 id;
	KEY_BLOCK_FMT format;
	ST_KEY_Attr * keyattr;
}ST_API_Store_Key;

typedef struct{
	u32 version;
	u32 keyindex;
	u8 *iv;
	u32 enc_or_dec;
	u32 mode;
	u32 dukpt_key_select;
}ST_API_DataCalc_Key;

typedef  enum {
	DATA_DEC,
	DATA_ENC
}enc_or_dec;

typedef  enum {
	DATA_MODE_ECB,
	DATA_MODE_CBC,
	DATA_MODE_OFB,
	DATA_MODE_CFB
}des_mode;


typedef struct{
	u32 version;
	u32 keyindex;
	u32 gen_or_verify;
	u32 mac_alg;
	u32 mac_padding;
	u8* mac_iv;
	u8  mac_in[8];
	u32 dukpt_key_select;	
}ST_API_MAC_KEY;

typedef  enum {
	MAC_GENERATE_MAC,
	MAC_VERIFY_MAC
}gen_or_verify;

typedef  enum {
	MAC_ALG_ISO_9797_1_MAC_ALG1,
	MAC_ALG_ISO_9797_1_MAC_ALG3,
	MAC_ALG_ISO_16609_MAC_ALG1,
	MAC_ALG_FAST_MODE,
	MAC_ALG_X9_19,
	MAC_ALG_CBC
}mac_alg;

typedef  enum {
	MAC_PADDING_MODE_1,
	MAC_PADDING_MODE_2
}mac_padding;

typedef struct{
	u32 version;
	u32 keyindex;
	char * len_sets;
	u32 pinblock_fmt;
	u32 timeoutms;
	u8 * datain;
}ST_API_PIN_KEY;

typedef struct{
	u32 version;
	EM_SEC_CTRL_CODE ctrl_code;
	u32 keyindex;
}ST_API_CTRL_KEY;

typedef  enum {
	SEC_PIN_BLK_ISO_FMT0,
	SEC_PIN_BLK_ISO_FMT1,
	SEC_PIN_BLK_ISO_FMT3,
	SEC_PIN_BLK_EPS
}pinblock_fmt;

typedef struct{
    unsigned char  reserve[3];
    char           name[17];
    unsigned int   length;
}FAT_ITEM;


int dbg_putchar(u8 c);
int usb_tx(u8 c);
int udelay(u32 us);
int mdelay(unsigned int ms);
/**MSR**/
int LvosMsrRead(u32 hWnd,u8 *Track1,u32 *T1len,u8 *Track2,u32 *T2len,u8 *Track3,u32 *T3len);

/**Smart Card Reader**/
int LvosScPoll(u32 hWnd);
int LvosScActive(u32 hWnd, u8 *rsp);
int LvosScExchange(u32 hWnd, const C_APDU *c_apdu, R_APDU *r_apdu);
int LvosScRemoval(u32 hWnd);
int LvosScPinVerify(u32 hWnd, ST_SCPINKEY  *key, u16 * SWAB);

/**PED**/
int LvosStoreKey(u32 hWnd,u32 key_type,void* key,void *keydata);
int LvosDataCalc(u32 hWnd,u32 key_type,void* key,u8 *datain,u32 datainlen,u8* dataout,u32 *dataoutlen);
int LvosMacCalc(u32 hWnd,u32 key_type,void* key,u8 *datain,u32 datainlen,u8* dataout,u32 *dataoutlen);
int LvosGetPinBlock(u32 hWnd,u32 key_type,void* key,void *pinblock);
int LvosSecCtrl(u32 hWnd,u32 key_type,void* key,u8 *datain,u32 datainlen,u8* dataout,u32 *dataoutlen);
int SecOfflinePinVerify(u32 offline_type, ST_SCPINKEY *RsaKey, u8 *status_word);/**kernel use**/

/**Filesystem**/
int LvosFopen(const char *filename);
int LvosFread(int fd, u8 *data, u32 len);
int LvosFwrite(int fd, u8 *data,u32 len);
int LvosFclose(int fd);
int LvosFseek(int fd, int offset,u32 fromwhere);
int LvosFsize(const char *filename);
int LvosFremove(const char *filename);
int LvosFFreeSize(void);
int LvosFFileinfo(FAT_ITEM *items, u32 itemNum);

/**Communication**/
int LvosCommRequest(u32 hWnd);
int LvosCommConnect(u32 hWnd);
int LvosCommSend(u32 hWnd,u8 *src,u32 len);
int LvosCommRecv(u32 hWnd,u8 *dst,u32 len);
int LvosCommDisconnect(u32 hWnd);
int LvosCommRelease(u32 hWnd,u32 force);
int LvosCommClear(u32 hWnd,u32 flags);
int LvosCommCheck(u32 hWnd,u32 *Status);

/**UI**/
int LvosUiSetAttr(u32 hWnd, u32 type, u32 value);
int LvosUiSetArea(u32 hWnd,u32 hpixel,u32 vpixel, u32 width, u32 height);
int LvosUiPrintf(u32 hWnd,const char * fmt,...);
int LvosUiScanf(u32 hWnd);
int LvosUiDrawArea(u32 hWnd, u32 hpixel,u32 vpixel, u32 width, u32 height, u8* data);

/**System**/
int LvosSysCfgRead(char *keyword,char *attr,u32 attrLen);
int LvosSysGetTimer(ST_Timer_UNIT *timer,u32 timeout);
int LvosSysTimerLeft(ST_Timer_UNIT *timer);
void LvosSysDelayMs(u32 ms);
int LvosSysGetTime(SYS_TIME * time);
int LvosSysSetTime(SYS_TIME * time);
int LvosSysPower(u32 mode);
int LvosSysSoundPlay(char mode,char *param);
int LvosSysRandom(u8 *rand,u32 len);
u32 LvosSysTick();
int LvosAppExecute(const char * appName);

/**User Paramters**/
int LvosParamEnum(u32 hWnd,u32 index, char *keyword,char *attr,u32 attrLen);
int LvosParamRead(u32 hWnd,const char *keyword,char *attr,u32 attrLen);
int LvosParamWrite(u32 hWnd,const char *keyword,char *attr);

/**Alg**/
int LvosDes(u8 *indata, u8 *outdata, u8 *key, u32 mode);
int LvosAes(u8 *indata, u8 *outdata, u8 *key, u32 keylen, u32 mode);
int LvosHash(u8* DataIn, u32 DataInLen, u8* DataOut);
int LvosSha(u32 type,u8 *HashOut, u32 DataLen, u8 *DataIn);
int LvosRsaRecover(u8 *mod, u32 modlen,u8 *exp, u32 explen,u8 *indata, u8 *outdata);
int LvosSm3( u8 *src,u32 len,u8 out[32]);
int LvosSm4(u8 *iv, u8 *datain, u8 *dataout, u32 datalen, u8 *key,u32 mode, u32 cMode);

#endif

