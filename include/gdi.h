#ifndef _GDI_H_
#define _GDI_H_

#define INVALID_DC 0xFFFFFFFF

int gdi_create_mem_dc(u32 width, u32 height);

void gdi_begin_paint(int dc);

void gdi_end_paint(int dc, int x, int y);

void gdi_close_mem_dc(int dc);

void gdi_draw_text (int dc, int x, int y, char *txt);

int gdi_draw_image(int dc, int x, int y, u8 *png, u32 len);

int gdi_draw_rectangle(int dc, int x, int y, u32 width, u32 height, u16 color);

void gdi_set_back_color(int dc, u16 color);

void gdi_set_pen_color(int dc, u16 color);

u16 gdi_get_back_color(int dc);

u16 gdi_get_pen_color(int dc);

void frame_buffer_write(int x, int y, int width, int height, u16 *data, u32 len);



int gdi_draw_depth1_image(int dc, int x, int y, u8 *img, u32 len);

#endif 
