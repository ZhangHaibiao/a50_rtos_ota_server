
#ifndef __UPDATE_H_
#define __UPDATE_H_


#define PUK_MOD_LEN             (256)
#define PUK_EXP_LEN             (4)
#define PUK_SIGN_LEN            (256)
#define PUK_ALL_LEN             (PUK_MOD_LEN+PUK_EXP_LEN)

#define SOC_FLASH_ADDR          0x14000000
#define SOC_RAM_ADDR            0x06100000

#define LITEOS_HEAD_MAGICNUM	(0xA7260101)
#define APP_HEAD_MAGICNUM	    (0xA7260102)


#define OS_START_ADDR   	 (48*1024)
#define OS_SIZE  			 (48*1024)

#define APP_START_ADDR   	 (192*1024)


typedef struct {
    u32 magicnum;
    u32 cid;
    u32 entry_addr;
    u32 load_addr;
    u32 contxt_len;
    u32 sign_alg;
    u8  puk[PUK_ALL_LEN];
    u32 reserve;               // reserve for fill Api Base Addr
    u8 sign[PUK_SIGN_LEN];
} IMAGE_HEAD; /* 288 + 256= 544 */


int CMD_SysUpdateProc(u8 *data, u32 len);


#endif

