
#ifndef __RESOURCE_H_
#define __RESOURCE_H_


/*
 * success 0, failed -1
 */
 
int ResGetTagInfo(char* tag, u32 *value_addr, u32 *value_len);
int ResGetTagInfoEx(u8 *pstart_addr, char* tag, u32 *value_addr, u32 *value_len);
int ResGetTagInfoByIDEx(u8 *pstart_addr, int id, char *name, u32 *value_addr, u32 *value_len);
int ResGetTagCountEx(u8 *pstart_addr);
int ResCheck(void);
int ResCheckEx(u8 *pstart_addr);

#endif

