

#ifndef __ADS_H_
#define __ADS_H_

int update_ads(u8 *data, u32 len);

int play_ads();

int set_ads_play_param(int idx, int interval, int mode, int area);


#endif
