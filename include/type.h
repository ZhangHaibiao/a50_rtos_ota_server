#ifndef __TYPE_H__
#define __TYPE_H__

#ifndef byte
typedef unsigned char byte;
#endif

#ifndef uint
typedef unsigned int uint;
#endif

#ifndef ushort
typedef unsigned short ushort;
#endif

#ifndef ulong
typedef unsigned long	ulong;
#endif

#endif

