#ifndef _LOG_H_
#define _LOG_H_


//#define DBG_ENABLE
//#define UART_DBG 

#ifdef DBG_ENABLE



void uart_debug(const char *str, ...);
int uart_hex_dump(void* buf, u32 len);
void usb_debug(const char *str, ...);
int usb_hex_dump(void* buf, u32 len);


#ifdef UART_DBG 


#define DBG_STR(fmt, args...) uart_debug("%s:%d "fmt"\r\n", __FUNCTION__, __LINE__, ##args);
#define DBG_HEX(title, data, len) {DBG_STR(title); uart_hex_dump(data, len);}
#define DBG_DAC(title, data, len)
#else
#define DBG_STR(fmt, args...) usb_debug("%s:%d "fmt"\r\n", __FUNCTION__, __LINE__, ##args);
#define DBG_HEX(title, data, len) {DBG_STR(title); usb_hex_dump(data, len);}
#define DBG_DAC(title, data, len) {DBG_STR(title); usb_hex_dump(data, len);}
#endif

#else

#define DBG_STR(fmt, args...) 
#define DBG_HEX(title, data, len) 
#define DBG_DAC(title, data, len)

#endif



#endif 
