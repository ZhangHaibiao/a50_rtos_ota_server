#ifndef __UTILS_H__
#define __UTILS_H__

#define MAX_BARCODE_HEIGHT 80

#define MAX_TAG_LEN     2
#define MAX_TAG_LEN_LEN 4
#define MAX_TAG_VAL_LEN 255

typedef struct
{
  ushort tag;
  byte   dataSize;
  byte * data;
  byte * dataLen;
  byte * flag;
}ST_EMVTAG;

/**
* @fn Utils_Int2Hex
* @brief  Int型数据转换为HEX码
* @param in   IntData:  待转换的Int型数据
* @param in   HexLen   转换后HEX码数据长度
* @param out  HexBuf   转换后HEX数据
* @return
* @li  0      成功
* @li  -1     失败
* @补充说明: 
*/
int Utils_Int2Hex(uint IntData, byte * HexBuf, int HexLen);

/**
* @fn Utils_Hex2Int
* @brief  HEX码数据转换为int型
* @param in  HexBuf:   待转换的HEX数据
* @param in  HexLen   HEX码数据长度
* @return
* @li  0	 成功
* @li  -1    失败
*/
uint Utils_Hex2Int(byte *HexBuf,  int HexLen);

/**
* @fn Utils_Int2Bcd
* @brief  Int型数据转换为BCD码
* @param in   IntData:  待转换的Int型数据
* @param out  BcdBuf   转换后BCD数据
* @param in   BcdLen   转换后BCD码数据长度
* @return
* @li  0      成功
* @li  -1     失败
* @补充说明: 
*/
int Utils_Int2Bcd(uint IntData, byte * BcdBuf, int BcdLen);

/**
* @fn Utils_Bcd2Int
* @brief  BCD码数据转换为int型
* @param in  BcdBuf:   待转换的BCD数据
* @param in  BcdLen   BCD码数据长度
* @return
* @li  0	 成功
* @li  -1    失败
*/
uint Utils_Bcd2Int(byte *BcdBuf,  int BcdLen);

/**
* @fn Utils_Bcd2Asc
* @brief  ASCII码转BCD码
* @param in  bcd      需进行转换的BCD码数据
* @param in  bcdLen   传入的BCD码数据长度，即转化ASCII码数据的一半长度
* @param out asc      转换输出的ASCII码数据
* @return
* @li  0       成功
* @li  -1      失败
* @补充说明: 左靠BCD码，位数不足后补''F'
*/
int Utils_Bcd2Asc(byte *bcd, uint bcdLen, byte *asc);

/**
* @fn Utils_Asc2Bcd
* @brief  ASCII码转BCD码
* @param in  asc      需进行转换的ASCII码数据
* @param in  ascLen   传入的ASCII码数据长度，即转化BCD码数据的双倍长度
* @param out bcd      转换输出的BCD码数据
* @return
* @li  0       成功
* @li  -1      失败
* @补充说明: 左靠BCD码，位数不足后补''F'
*/
int Utils_Asc2Bcd(byte *asc, uint ascLen, byte *bcd);

/**
* @fn Utils_Int2Str
* @brief  数值转数字字符串
* @param in  iVal: 数值
* @param out str: 正数数字字符串
* @return
* @li   0 成功
* @补充说明: 同 itoa
*/
int Utils_Int2Str(int iVal, char * str, uint len);

/**
* @fn Utils_Str2Int
* @brief  正数数字字符串转数值
* @param in  str: 数字字符串
* @return
* @li    数值
* @补充说明: 同 atoi
*/
int Utils_Str2Int(const char * str);

/**
* @fn Utils_Int2Amt
* @brief  金额数值（分）转为金额字符串（元）
* @param in  amt: 金额数值（分）
* @param out strAmt: 金额字符串（元）
* @return
* @li  0  金额数值（分）
* @补充说明: 12300->"123.00"
*/
void Utils_Int2Amt(uint amt, char * strAmt);

/**
* @fn Utils_Amt2Int
* @brief  金额字符串（元）转为金额数值（分）
* @param in  strAmt: 金额字符串（元）
* @return
* @li  0  金额数值（分）
* @补充说明: "123"->12300,"123.01"->12301
*/
uint Utils_Amt2Int(const char * strAmt);

/**
* @fn Utils_Amt2Asc
* @brief  金额字符串（元）转为金额字符串（分）
* @param in  strAmt: 金额字符串（元）
* @param out ascAmt: 金额字符串（分）
* @param in  amtSize: 分的长度
* @return
* @li  0  成功
       -1 失败
* @补充说明: "123"->"12300","123.01"->"12301"
*/
int Utils_Amt2Asc(const char * strAmt, char * ascAmt, int amtSize);

/**
* @fn Utils_Asc2Amt
* @brief  金额字符串（分）转为金额字符串（元）
* @param in  ascAmt:金额字符串（分）
* @param out strAmt:金额字符串（元）
* @param in  strSize:元的长度
* @return
* @li  0  成功
       -1 失败
* @补充说明: "12300"->"123.00","12301"->"123.01"
*/
int Utils_Asc2Amt(const char * ascAmt, char * strAmt, int strSize);

/**
* @fn Util_StrCopy
* @brief  中文智能截断函数, 解决一行中文显示半个汉字的问题
* @param in  scr:源数据串
* @param in  len:源数据长度
* @param out Dst:目标数据串
* @return
* @li  返回已拷贝的字符串长度
* @补充说明:  此函数有例子见相关文档
*/
int Utils_StrCopy(char * dst, const char *src, int len);

byte Utils_AscAdd(byte *augend, byte *addend, uint len);

byte Utils_AscSub(byte *minuend, byte *subtrahend, uint len);

void Utils_Xor(byte *psVect1, byte *psVect2, uint uiLength, byte *psOut);


int Uitls_AddEMVTLV(byte * data, uint dataLen, ushort tag, byte *val, uint valLen);
int Utils_DelEMVTLV(byte * data, uint dataLen, ushort tag);
int Utils_ModifiyTLV(byte * data, uint dataLen, ushort tag, byte *newVal, uint newValLen);
int Utils_GetEMVTLV(byte * data, uint dataLen, ushort tag, byte * val, uint valSize);
int Utils_DecodeEMVTlv(byte * data, uint dataLen, const ST_EMVTAG * tagList, uint tagNum);

void Utils_Escape(const char * src, char * dst);

char * Utils_TrimStr(char *pszString);

int Utils_IsLeapYear(int year);

int Utils_DayOfMonth(int month, int year);

int Utils_GBK2UTF8(const     char *pGBK, char *pUTF8, int number);

int Utils_UTF82GBK(const char *pUTF8, char *pGBK, int number);

int Utils_IsTextUTF8(const char * str, int length);

int Utils_GetHandle(const char * pdev_name);


//#define DBG_STR(fmt, args...) {usb_debug("%s:%d "fmt"\r\n", __FUNCTION__, __LINE__, ##args); LvosSysDelayMs(10);}
//#define DBG_HEX(title, data, len) {DBG_STR(title); LvosSysDelayMs(10); app_printhex(data, len);}

//extern int uart_hex_dump(void* buf, u32 len);
//#define DBG_STR(fmt, args...) {uart_debug("%s:%d "fmt"\r\n", __FUNCTION__, __LINE__, ##args);}
//#define DBG_HEX(title, data, len) {DBG_STR(title);  uart_hex_dump(data, len);}
#endif

