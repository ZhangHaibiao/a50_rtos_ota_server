#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "liteos_api.h"
#include "log.h"

extern u32 _Resource_Flash_addr;

// 检查RESOURCE是否存在,检查RESOURCEHASH是否正确
static int _ResCheck(u8 *pstart_addr)
{
	//u8 *pstart_addr = (u8 *)&_Resource_Flash_addr;
	u32 magic_code = 0;
	u32 data_offset = 0;
	u32 data_len = 0;
	u8 hash_in[32], hash_cal[32];

	DBG_STR("start_addr:%08x\r\n", pstart_addr);
	memcpy(&magic_code, pstart_addr, 4);
	DBG_STR("magic code:%08x\r\n", magic_code);
	if(magic_code != 0xA7260001) return -1;
	memcpy(&data_offset, pstart_addr + 4, 4);
	memcpy(&data_len, pstart_addr + 8, 4);
	memcpy(hash_in, pstart_addr + 12, 32);
	DBG_STR("data_offset:%08x,data_len:%d\r\n", data_offset, data_len);
	//uart_debug("hash in:\r\n");
	//usb_hex_dump(hash_in, 32);

	// cal hash
	LvosSha(2, hash_cal, data_len, pstart_addr + data_offset);
	//DBG_STR("hash cal:\r\n");
	//usb_hex_dump(hash_cal, 32);

	if(memcmp(hash_in, hash_cal, 32) != 0) return -2;

	return 0;
}

int ResCheck()
{
	u8 *pstart_addr = (u8 *)&_Resource_Flash_addr;

	return _ResCheck(pstart_addr);
}

int ResCheckEx(u8 *pstart_addr)
{
	return _ResCheck(pstart_addr);
}

static int _ResGetTagInfo(u8 *pstart_addr, char* tag, u32 *value_addr, u32 *value_len)
{
	//u8 *pstart_addr = (u8 *)&_Resource_Flash_addr;
	u32 magic_code = 0;
	u32 data_offset = 0;
	u32 data_len = 0, cur_offset = 0;
	char tmp_tag[16];
	u32 tmp_tag_len;

	memcpy(&magic_code, pstart_addr, 4);
	//uart_debug("magic code:%08x\r\n", magic_code);
	if(magic_code != 0xA7260001) return -1;
	memcpy(&data_offset, pstart_addr + 4, 4);
	memcpy(&data_len, pstart_addr + 8, 4);
	//uart_debug("data_offset:%08x,data_len:%d\r\n", data_offset, data_len);

    //uart_debug("tag %s \n", tag);

	pstart_addr += data_offset;

	while(cur_offset < data_len)
	{
		memcpy(tmp_tag, pstart_addr + cur_offset, 16);
		cur_offset += 16;
		memcpy(&tmp_tag_len, pstart_addr + cur_offset, 4);
		cur_offset += 4;
		if(strcmp(tmp_tag, tag) == 0)
		{
			//uart_debug("addr:%08x, len:%d\r\n", (u32)(pstart_addr + cur_offset), tmp_tag_len);
			if(value_addr != NULL) *value_addr = (u32)(pstart_addr + cur_offset);
			if(value_len != NULL)*value_len = tmp_tag_len;
			return 0;
		}
		cur_offset += tmp_tag_len;
	}

	return -1;
}

int ResGetTagInfo(char* tag, u32 *value_addr, u32 *value_len)
{
    u8 *pstart_addr = (u8 *)&_Resource_Flash_addr;
    
    return _ResGetTagInfo(pstart_addr, tag, value_addr, value_len);
}

int ResGetTagInfoEx(u8 *pstart_addr, char* tag, u32 *value_addr, u32 *value_len)
{
    return _ResGetTagInfo(pstart_addr, tag, value_addr, value_len);
}

int ResGetTagInfoByIDEx(u8 *pstart_addr, int id, char *name, u32 *value_addr, u32 *value_len)
{
	//u8 *pstart_addr = (u8 *)&_Resource_Flash_addr;
	u32 magic_code = 0;
	u32 data_offset = 0;
	u32 data_len = 0, cur_offset = 0;
    char tmp_tag[16];
	u32 tmp_tag_len;
    int idx = 0;

	memcpy(&magic_code, pstart_addr, 4);
	//uart_debug("magic code:%08x\r\n", magic_code);
	if(magic_code != 0xA7260001) return -1;
	memcpy(&data_offset, pstart_addr + 4, 4);
	memcpy(&data_len, pstart_addr + 8, 4);
	//uart_debug("data_offset:%08x,data_len:%d\r\n", data_offset, data_len);

    //uart_debug("tag %s \n", tag);

	pstart_addr += data_offset;

	while(cur_offset < data_len)
	{
        memcpy(tmp_tag, pstart_addr + cur_offset, 16);
        tmp_tag[sizeof(tmp_tag) - 1] = '\0';
		cur_offset += 16;
		memcpy(&tmp_tag_len, pstart_addr + cur_offset, 4);
		cur_offset += 4;
		if(idx == id)
		{
			//uart_debug("addr:%08x, len:%d\r\n", (u32)(pstart_addr + cur_offset), tmp_tag_len);
			if (name != NULL) strcpy(name, tmp_tag);
			if(value_addr != NULL) *value_addr = (u32)(pstart_addr + cur_offset);
			if(value_len != NULL)*value_len = tmp_tag_len;
			return 0;
		}
		cur_offset += tmp_tag_len;
        idx++;
	}

	return -1;

}

int ResGetTagCountEx(u8 *pstart_addr)
{
	u32 magic_code = 0;
	u32 data_offset = 0;
	u32 data_len = 0, cur_offset = 0;
	u32 tmp_tag_len;
    int cnt = 0;

	memcpy(&magic_code, pstart_addr, 4);
	//uart_debug("magic code:%08x\r\n", magic_code);
	if(magic_code != 0xA7260001) return -1;
	memcpy(&data_offset, pstart_addr + 4, 4);
	memcpy(&data_len, pstart_addr + 8, 4);

	pstart_addr += data_offset;

	while(cur_offset < data_len)
	{
		cur_offset += 16;
		memcpy(&tmp_tag_len, pstart_addr + cur_offset, 4);
		cur_offset += 4;
		cur_offset += tmp_tag_len;

        cnt++;
	}

	return cnt;    
}


