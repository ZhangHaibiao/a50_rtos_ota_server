#ifndef __CMDPROC_H_
#define __CMDPROC_H_

#define TYPE_LCD        (0x01)
#define TYPE_BEEP       (0x02)
#define TYPE_LED        (0x03)

#define TRANS_QR_DISP        (0x01)
#define TRANS_OK_DISP        (0x02)
#define TRANS_FAIL_DISP      (0x03)

#define LCD_CLEAR                (0x0100)
#define LCD_TRANS_QR_DISP        (0x0101)
#define LCD_TRANS_OK_DISP        (0x0102)
#define LCD_TRANS_FAIL_DISP      (0x0103)
#define LCD_TRANS_QR_STR_DISP    (0x0111)
#define LCD_BACKLIGHT            (0x0120)
#define LCD_ADS_DISP             (0x0121)


#define SYS_READ_CONFIG          (0x0400)
#define SYS_OTA_UPDATE           (0x0401)
#define SYS_ADS_UPDATE           (0x0402)
#define SYS_TRANSMIT_FILE           (0x0403)



#define DL_TIME_OUT             (100)   //ms
#define B_TIMEOUT 1000 //50		//ms 

#define COM_MAX_BUFF_LEN        (63*1024+8)

enum {UART_DL=0,USB_PORT=0xF0};

typedef struct 
{
    u8 head;               
    u8 seq;               
    u8 type;
    u8 opscode;             
    u8 datalen[2];
    u8 *pdata;
    u8 checksum;
}DATA_PACKAGE;

typedef enum {
    RET_OK = 0,
    RET_ERR_INVAL=0x10,
    RET_ERR_HASH,
    RET_ERR_SIGN,
    RET_ERR_MAGIC,
    RET_ERR_PUKFLG,
    RET_ERR_PUKCRC,
    RET_ERR_DL_TO,
    RET_ERR_DL_NODATA,
    RET_ERR_AMT_LEN,
    RET_ERR_LEN,
} ret_errors;    

#define PAY_STA_IDLE      (0)
#define PAY_STA_OK        (1)
#define PAY_STA_FAIL      (2)
#define PAY_STA_DOING     (3)
#define PAY_STA_ADS       (4)


#define LCD_W_MAX       (320)
#define LCD_H_MAX       (480)

#define CUSTOM_BACK_COLOR  (0x09f9)
#define PAY_QR_START_H     (120)
#define PAY_SCAN3_H     (32)
#define PAY_FONT_W      (24)
#define PAY_FONT_H      (48)
#define PAY_AMT_W       (28)
#define PAY_AMT_H       (34)
#define PAY_AMT_PAD      ((PAY_FONT_H-PAY_AMT_H)/2)
#define PAY_AMT_LEN     (11)
#endif // __CMDPROC_H_
