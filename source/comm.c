
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "liteos_api.h"
#include "type.h"
#include "log.h"

#include "comm.h"


#define COMM_DEBUG

#ifdef COMM_DEBUG
#define COMM_STRLOG DBG_STR
#define COMM_HEXLOG DBG_HEX
#else
#define COMM_STRLOG  
#define COMM_HEXLOG
#endif

#define TMP_SIZE 1024

static int m_CurCommMode;
static int m_CommHandle = -1;
static uint m_uiCommSignal = 0;
static uint m_SignalFresh = 1000; 

static byte isShowPrompt = 1;


int Comm_GetHandle(const char * device)
{
	int iRet;
	
	iRet = Sys_GetHandle(device);
	COMM_STRLOG("Sys_GetHandle(%s) = %d", device, iRet);
	return iRet;
}

int Comm_WriteParam(int handle, const char * param, const char * buffer)
{
	int iRet;
	
	iRet = Sys_WriteParam(handle, param, buffer);
	COMM_STRLOG("Sys_WriteParam(%d, %s, %s) = %d", handle, param, buffer, iRet);
	return iRet;
}

int Comm_ReadParam(int handle, const char * param, char * buffer, uint size)
{
	int iRet;
	
	iRet = Sys_ReadParam(handle, param, buffer, size);
	COMM_STRLOG("Sys_ReadParam(%d, %s, %s) = %d", handle, param, buffer, iRet);
	return iRet;
}


void Comm_Init(int flag)
{
    m_CommHandle = Comm_GetHandle("WLAN0");
}

int Comm_ConnectPPP(uint timeMS, uint start, uint isCancel)
{
	int iRet;
	int iLeft = 0;
	int pppStart = 0;
	int isWifiEnable = 0;
	int isConnectAp = 0;
	int isPPP = 0;

	pppStart = LvosSysTick();
	
	while(1)
	{
		uint status=0;		
		iRet = LvosCommCheck((uint)m_CommHandle, &status);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommCheck(%d, %d) = %d", m_CurCommMode, m_CommHandle, iRet);
			return COMM_RET_FAIL;
		}
        
		if(status & 0x08) {
			COMM_STRLOG("GPRS Ready");
			return COMM_RET_OK;
		}
        
		if(!isPPP) {
			iRet = LvosCommRequest((uint)m_CommHandle);
			COMM_STRLOG("LvosCommRequest(%d, %d) = %d", m_CurCommMode, m_CommHandle, iRet);
			if(iRet >= 0) {
				isPPP ++;
			}
		}

		iRet = LvosSysTick();
		if(timeMS < (iRet -pppStart)) {
			return COMM_RET_FAIL;
		}
		
		LvosSysDelayMs(200);
	}
	
FAILED:

	return COMM_RET_FAIL;
}

int Comm_ConnectHttpHost(uint timeMS, uint start, uint isCancel, const char * host, const char * port)
{
	int iRet;
	uint tcpStart;
	int  iLeft;

	Sys_WriteParam(m_CommHandle, "SERVER_IP", host);
	COMM_STRLOG("SERVER_IP = %s", host);
	Sys_WriteParam(m_CommHandle, "SERVER_PORT", port);
	COMM_STRLOG("SERVER_PORT = %s", port);
	
	iRet = LvosCommConnect(m_CommHandle);
	if(iRet < 0) {
		COMM_STRLOG("LvosCommConnect(%d) = %d", m_CommHandle, iRet);
		return COMM_RET_FAIL;
	}
	
	tcpStart = LvosSysTick();
	while(1)
	{
		uint status=0;
		
		iRet = (timeMS + start - LvosSysTick()) / 1000;
		if(timeMS > iRet && iLeft != iRet) {
			char left[4];
			iLeft = iRet;
			sprintf(left, "%02d", iLeft);
		}
		
		iRet = LvosCommCheck((uint)m_CommHandle, &status);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommCheck(%d) = %d", m_CommHandle, iRet);
			return COMM_RET_FAIL;
		}
		
		if(status & 0x01) {
			COMM_STRLOG("status = 0x%x", status);
			return m_CommHandle;
		}
		
		iRet = LvosSysTick();
		
		if(timeMS < (iRet - tcpStart)) {
			return COMM_RET_TIMEOUT;
		}
				
		LvosSysDelayMs(100);
	}
	
	return COMM_RET_FAIL;
}



int Comm_ConnectHttp(uint timeMS, uint isCancel, const char * host, const char * port)
{
	int iRet = 0;

	iRet = Comm_ConnectPPP(timeMS, 0, isCancel);
	if(iRet != COMM_RET_OK) {
		return -1;
	}
	
	iRet = Comm_ConnectHttpHost(timeMS, 0, isCancel, host, port);
	if(iRet >= 0) {
		return iRet;
	}
	
	return iRet;
}


int  Comm_SendHttp(byte * data, uint dataLen)
{
	return Comm_Send(data, dataLen);
}

int  Comm_RecvHttp(byte * data, uint dataSize)
{
	return LvosCommRecv((uint)m_CommHandle, data, dataSize);
}

int  Comm_Send(byte * data, uint dataLen)
{
	int iRet;
	int sendLen;
	char left[4];

	for(sendLen = 0; sendLen < dataLen; ) {
		if((dataLen - sendLen) > 1024) {
			iRet = LvosCommSend((uint)m_CommHandle, data + sendLen, 1024);
		} else {
			iRet = LvosCommSend((uint)m_CommHandle, data + sendLen, dataLen - sendLen);
		}
		if(iRet < 0) {
			COMM_STRLOG("LvosCommSend(%d) = %d", dataLen, iRet);
			return COMM_RET_FAIL;
		}
		sendLen += iRet;
	}
	return dataLen;
}

int  Comm_Recv(byte * data, uint dataSize, uint timeMS, uint isCancel)
{
	int iRet;
	int dataLen;
	int recvLen;
	uint start;
	int iLeft;
	char left[4];
	
	iLeft = 0;
	start = LvosSysTick();
	for(dataLen = 0, recvLen = 0;  recvLen < dataSize; ) {		
		iRet = LvosSysTick() - start;
		if(timeMS > iRet && iLeft != (timeMS -iRet) / 1000) {
			iLeft = (timeMS -iRet) / 1000;
			sprintf(left, "%02d", iLeft);
		}
		if(timeMS < iRet) {
			iRet = COMM_RET_TIMEOUT;
			goto FAIL;
		}
		iRet = LvosCommRecv((uint)m_CommHandle, data + recvLen, dataSize - recvLen);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommRecv = %d", iRet);
			iRet = COMM_RET_FAIL;
			goto FAIL;
		}
		recvLen += iRet;
		if(recvLen < 2) {
			continue;
		}
		dataLen = Utils_Hex2Int(data, 2) + 2;
		if(recvLen < dataLen) {
			continue;
		}
		return dataLen;
	}
	
FAIL:
	switch(iRet)
	{
	case COMM_RET_TIMEOUT:
		break;
	
	case COMM_RET_CANCEL:
		return iRet;
		
	default:

		break;
	}
	
	return iRet;
}

void Comm_Close()
{	
	int iRet;
	uint start;
	
	iRet = LvosCommDisconnect((uint)m_CommHandle);
	COMM_STRLOG("LvosCommDisconnect(%d) = %d", m_CommHandle, iRet);
	start = LvosSysTick();
	
	while(1) {
		uint status=0;
		
		iRet = LvosCommCheck((uint)m_CommHandle, &status);
		if(iRet < 0) {
			COMM_STRLOG("LvosCommCheck(%d) = %d", m_CommHandle, iRet);
			return;
		}
		
		if( !(status & 0x01) ) {
			return;
		}
		
		iRet = (LvosSysTick() - start);
		if(2000 < iRet) {
			return;
		}
		
		LvosSysDelayMs(100);
	}
	
	return;
}


