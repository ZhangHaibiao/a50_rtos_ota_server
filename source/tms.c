#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "liteos_api.h"
#include "type.h"
#include "log.h"

#include "http.h"

#define TMS_DEBUG 1

#ifdef TMS_DEBUG
#define TMS_STRLOG DBG_STR
#define TMS_HEXLOG DBG_HEX
#else
#define TMS_STRLOG  
#define TMS_HEXLOG 
#endif

#define URL_LEN 256
#define DATA_LEN 1024


#define FILE_NAME "app_update"
static int g_fd;

// 一次下载8K
#define TMS_DOWNLOAD_SIZE 		   8192	

static void Tms_ShowProgress(int ratio)
{
}

static int Tms_DecodeUrl(const char *url, char * host, char * port, char *path)
{
	int iRet;
	char tmp[12];
	char * pUrl = NULL;
	char * pPort = NULL;
	char * pSplit = NULL;

	TMS_STRLOG("url: %s", url);
	pUrl = strstr(url, "http://");
	if(pUrl == NULL) {
		pUrl = strstr(url, "https://");
		if(pUrl == NULL) {
			pUrl = (char *)url;
		} else {
			pUrl += 8;
		}
	} else {
		pUrl += 7;
	}
	
	pSplit =  strstr(pUrl, "/");
	if(pSplit == NULL) {
		if(strlen(pUrl) > 256) {
			return -1;
		}
		strcpy(host, pUrl);
	} else {
		if(strlen(pSplit) > 256) {
			return -1;
		}
		iRet = (uint)pSplit - (uint)pUrl;
		if(iRet > 256 || iRet < 0) {
			return -1;
		}
		strcpy(path, pSplit);
		memcpy(host, pUrl, iRet);
	}
	pPort = strstr(host, ":");
	if(pPort == NULL) {
		strcpy(port, "80");
	} else {
		*pPort = 0x00;//截断IP地址20220321
		
		pPort++;
		if(strlen(pPort) > 5) {
			return -1;
		}
		strcpy(port, pPort);
	}
	
	TMS_STRLOG("host: %s", host);
	TMS_STRLOG("port: %s", port);
	TMS_STRLOG("path: %s", path);

	return 0;
}


// 保存数据到FLASH
static int Tms_WriteData(void *buf, int len, void * param)
{
    int ret;
    ret = LvosFwrite(g_fd, buf, len);
    if (ret < 0) {
        DBG_STR("write file error fd %d,  %d", g_fd, ret);
        LvosFclose(g_fd);
        g_fd = -1;
        return -1;
    }

    return 0;
}

static int open_file()
{
    if (g_fd >= 0) {
        LvosFclose(g_fd);
        g_fd = -1;
    }
    
    LvosFremove(FILE_NAME);
    
    DBG_STR("start write file, freesize %d", LvosFFreeSize());
    
    g_fd = LvosFopen(FILE_NAME);
    if (g_fd < 0) {
        DBG_STR("open failed %d", g_fd);
        return -2;
    }

    return 0;
}

static void close_file()
{
    LvosFclose(g_fd);
    g_fd = -1;
}


static int Tms_Download()
{
	int iRet;
	int total = 0;
	int down_len = 0;
	char host[256];
	char port[10];
	char path[256];
	char tmp[100];
	char *url = "http://althico-oss.oss-cn-shenzhen.aliyuncs.com/app_path/ddfea183e51e97b94ff9c7a45e347594";
    int file_size = 282624;
	HTTPHANDLE request;
	
	TMS_STRLOG("URL:%s\n uFileSize:%d\n offset:%d",url, file_size, 0);
	
	memset(host, 0, sizeof(host));
	memset(port, 0, sizeof(port));
	memset(path, 0, sizeof(path));
	iRet = Tms_DecodeUrl(url, host, port, path);
	if(iRet < 0 || strlen(host) == 0 || strlen(port) == 0 || strlen(path) == 0) {
	    TMS_STRLOG("Http_Connect = %d", iRet);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	iRet = Http_InitHandle(&request, TMS_DOWNLOAD_SIZE + 512);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitHandle = %d", iRet);
		sprintf(tmp, "初始化句柄错误[%d]", iRet);
		iRet = HTTP_ERR_DATA;
		return iRet;
	}
	
	iRet = Http_Connect(&request, host, port, 15 * 1000);
	//iRet = Http_Connect(&request, "120.77.167.197", port, 15 * 1000);
	if(iRet < 0) {
	    TMS_STRLOG("Http_Connect = %d", iRet);
		sprintf(tmp, "连接后台失败[%d]", iRet);
		iRet = HTTP_ERR_TIMEOUT;
		goto FINISH;
	}
	
	request.socket = iRet;
	Tms_ShowProgress(0);
    open_file();
    
DOWNLOAD:
	request.dataLength = 0;
	request.headerLength = 0;
	request.isTrunked = 0;
	request.trunckedDataLen = 0;
	request.packLen = 0;
	memset(request.pack, 0, request.packSize);
	
	iRet = Http_InitRequest(&request, "GET", host, port, path);
	if(iRet != HTTP_SUCCESS) {
    	TMS_STRLOG("Http_InitRequest = %d", iRet);
		sprintf(tmp, "初始化请求出错[%d]", iRet);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	Http_SetHeader(&request, "Connection", "Keep-Alive");
	Http_SetHeader(&request, "Cache-Control", "no-cache");
	Http_SetHeader(&request, "Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
	Http_SetHeader(&request, "Accept", "*/*");
	Http_SetHeader(&request, "User-Agent", "althcio");
	
	if ((file_size - total) > TMS_DOWNLOAD_SIZE) {
		down_len = TMS_DOWNLOAD_SIZE;
	} else {
		down_len = file_size - total;
	}
	
	memset(tmp, 0, sizeof(tmp));
	sprintf(tmp, "bytes=%d-%d", total, total + down_len - 1);
	TMS_STRLOG("Range: %s", tmp);
	
	Http_SetHeader(&request, "Range", tmp);
	
	Http_SetRequestBody(&request, "", 0);
	
	// TMS_HEXLOG("HTTP requset:\r\n%s", request.pack, request.packLen);
	Http_SetShowPrompt(0);

	iRet = Http_Send(&request);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Send = %d", iRet);
		sprintf(tmp, "发送数据失败[%d]", iRet);
		iRet = HTTP_ERR_SEND;
		goto FINISH;
	}
	
	iRet = Http_Recv(&request, APP_UI_TIMEOUT);
	if(iRet < 0) {
    	TMS_STRLOG("Http_Recv = %d", iRet);
		sprintf(tmp, "接收数据失败[%d]", iRet);
		iRet = HTTP_ERR_RECV;
		goto FINISH;
	}
	
	if(request.status != 200 && request.status != 206) {
    	TMS_STRLOG("Http_GetResponseCode = %d", request.status);
		sprintf(tmp, "状态错误[%d]", request.status);
		iRet = HTTP_ERR_STATUS;
		goto FINISH;
	}
	
	iRet = Http_DecodeResponseBody(&request, NULL, Tms_WriteData);
	if(iRet < 0) {
    	TMS_STRLOG("Http_DecodeResponseBody = %d", iRet);
		sprintf(tmp, "更新数据失败[%d]", iRet);
		iRet = HTTP_ERR_DATA;
		goto FINISH;
	}
	
	total += iRet;
	if(total < file_size) {
		// 未下载完成
		goto DOWNLOAD;
	}
	
	iRet = 0;

    DBG_STR("downlaod success");
	
FINISH:
	Http_Release(&request);
	
	if(iRet == HTTP_ERR_DATA) {
		// 数据错误删除断点信息
		return iRet;
	} 
	
	if(total < file_size) {
		// 未完成且下载次数未超过5次
		return -1;
	}
	
	return iRet;
}

int download_file()
{
    int ret;
    
    open_file();
    ret = Tms_Download();
    close_file();

    DBG_STR("result %d", ret);
    
    while (1) {
        //DBG_STR("down load ");
        LvosSysDelayMs(5000);
    }
}

