#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "type.h"
#include "liteos_api.h"
#include "log.h"
#include "update.h"



#define APP_UPDATE_ADDR 0x2000000
#define APP_MAX_SIZE (1120 * 1024)

#define OS_UPDATE_ADDR 0x1000000
#define OS_MAX_SIZE (170 * 1024)

//#define UPDATE_OS

int LvoslitEnable(char *key);
int LvoslitWriteFlash(u32, u8 *data, u32 len);



static int SysUpdate(u32 offset, u8*data, u32 datalen, u32 totallen)
{
#ifdef UPDATE_OS
    u32 start_addr = OS_UPDATE_ADDR;
#else
    u32 start_addr = APP_UPDATE_ADDR;
#endif
    u32 i;
    int ret=0;
    IMAGE_HEAD header;

    if (offset == 0) {     
        LvoslitEnable("lmtascrdenbal");        

        if(totallen > APP_MAX_SIZE) {
            return -2;
        }

        DBG_STR("start_addr:%08x, total: %d", start_addr, totallen);
    }

    ret = LvoslitWriteFlash(start_addr + offset, data, datalen);
    if (ret < 0) {
        return -1;
    }

    if ((offset + datalen) == totallen) {
        //u8 hash[32] = {0};
        //LvosSha(2, hash, totallen, (u8 *)(SOC_FLASH_ADDR + UPDATE_FILE_ADDR));
        //DBG_HEX("update", hash, sizeof(hash));
        

        return 1;
    }
    
    return 0;
}

int CMD_SysUpdateProc(u8 *data, u32 len)
{
    u32 datalen,offset,totallen;
    int ret;

    offset    = ((u32)data[0]<<24) + ((u32)data[1]<<16) + ((u32)data[2]<<8 ) + ((u32)data[3] );
    datalen   = ((u32)data[4]<<24) + ((u32)data[5]<<16) + ((u32)data[6]<<8 ) + ((u32)data[7] );
    totallen  = ((u32)data[8]<<24) + ((u32)data[9]<<16) + ((u32)data[10]<<8) + ((u32)data[11]);

    if (len != (datalen + 12)) {
        //DBG_STR("param err: %d,%d", paramslen, datalen+12);
        return -1;
    }

    ret = SysUpdate(offset, &data[12], datalen, totallen);
    if (ret < 0) {
        DBG_STR("data error ");
    }

    //DBG_STR("\r\noffset:%d, len:%d, totallen:%d, ret:%d --\r\n", offset, datalen, totallen, ret);
    return ret;
}







