#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include "liteos_api.h"
#include "litemapi.h"
#include "type.h"
#include "app.h"
#include "cmdproc.h"
#include "base64.h"
#include "log.h"
#include "ads.h"
#include "update.h"
#include "ui.h"

#define DL_DEBUG  0

#if DL_DEBUG
#define DL_STR DBG_STR
#else
#define DL_STR
#endif


static u32  s_dl_com = USB_PORT;
//static u32 g_is_play_ads;

static int s_PortRxs(u32 uart_id, u8 *buf, u32 len, u32 timeout)
{
    return PortRecvs(USB_DEV, buf, len, timeout);
}

static int s_PortTxs(u32 uart_id, u8 *buf, u32 len)
{
    if (PortSends(USB_DEV, buf, len) == 0) {
        return len;
    } else {
        return 0;
    }
}

static int s_PkgSend(u32 portid, DATA_PACKAGE* pkg, u32 timeout)
{
    ST_Timer_UNIT stTime;
    int iRet,i;
    u32 sendLen,datalen;
    u8 lrc, * pt = (u8*)pkg, *pbuf=NULL;
    
    datalen = pkg->datalen[0];
    datalen <<= 8;
    datalen |= pkg->datalen[1];

    lrc = 0;
    for(i = 0; i < 6; i++)
    {
        lrc ^= pt[i];
    }

    for(i = 0; i < datalen; i++)
    {
        lrc ^= pkg->pdata[i];
    }
    pkg->pdata[datalen] = lrc;

    if(datalen){
        pbuf = Sys_Malloc(datalen+7);
        if(pbuf==NULL) return -RET_ERR_INVAL;
        memcpy(pbuf, pt, 6);
        memcpy(pbuf+6, pkg->pdata, datalen);
        pbuf[datalen+6] = lrc;
        pt = pbuf;
    }

#if 0
    DL_STR("TX:");
    for (i = 0; i < 6+datalen; i++)
    {
        uart_debug("%02x ", pt[i]);
        if (0 == (i+1)%16)uart_debug("\r\n");
    }
#endif
	iRet = -3;

    LvosSysGetTimer(&stTime, timeout);
    i = 0;
    while(1)
    {
        i += s_PortTxs(portid, pt + i, datalen + 7 - i);
        DBG_STR("s_PortTxs i %d", i);
        if(i == datalen + 7) {iRet = 0; goto END;}
        if(!timeout) {iRet = -RET_ERR_DL_TO; goto END;}
        if(!LvosSysTimerLeft(&stTime)) {iRet = -RET_ERR_DL_TO; goto END;}
    }
END:

    if (pbuf != NULL) {
        Sys_Free(pbuf);
    }
    return iRet;
}


static int s_PkgRecv(u32 portid, DATA_PACKAGE* pkg, u32 timeout)
{
    ST_Timer_UNIT stTime;
    int i,j,iRet;
    u32 datalen;
    u8 ch, lrc, * pt = (u8*)pkg;
    
    // recv head
    LvosSysGetTimer(&stTime, timeout);
    while(1)
    {
        iRet = s_PortRxs(portid, &pt[0], 1, 0);
        if((iRet == 1) && (pt[0] == 0x02)) break;
        if(!timeout) return -RET_ERR_DL_TO;
        if(!LvosSysTimerLeft(&stTime)) return -RET_ERR_DL_TO;
    }

    // recv seq type opstype len
    for(i = 1; i < 6; i++)
    {
        iRet = s_PortRxs(portid, &pt[i], 1, B_TIMEOUT);
        if(iRet != 1) return -RET_ERR_DL_TO;
    }

    datalen = pkg->datalen[0];
    datalen <<= 8;
    datalen |= pkg->datalen[1];

    DL_STR("iRet=%d, datalen:%d", iRet, datalen);

    // datalen error
    if(datalen  > COM_MAX_BUFF_LEN)
    {
        DL_STR("datalen %d", datalen);
        return -RET_ERR_INVAL;
    }

    
    
    pkg->pdata = Sys_Malloc(datalen+1);
    if(datalen && (pkg->pdata==NULL)) return -RET_ERR_INVAL;
    pkg->pdata[datalen] = 0x00;

    // recv data
    for(i = 0; i < datalen; i++)
    {
        iRet = s_PortRxs(portid, &(pkg->pdata[i]), 1, B_TIMEOUT);
        if(iRet != 1) {
            DL_STR("i %d,  datalen %d \n", i, datalen);
            return -RET_ERR_DL_TO- 102;
        }
    }

    iRet = s_PortRxs(portid, &(pkg->checksum), 1, B_TIMEOUT);
    if(iRet != 1) {
        DL_STR("i %d,  datalen %d \n", i, datalen);
        return -RET_ERR_DL_TO- 102;
    }

    // check lrc
    lrc = 0;
    for(i = 0; i < 6; i++)
    {
        lrc ^= pt[i];
    }
//DBG_HEX("HEAD:", pt, 6);
    for(i = 0; i < datalen; i++)
    {
        lrc ^= pkg->pdata[i];
    }
#if 0
    if (datalen>12){
        DBG_HEX("data0:\r\n", pkg->pdata, 12);
        DBG_HEX("data1:\r\n", pkg->pdata+12, datalen-12);
    } else {
        DBG_HEX("data:\r\n", pkg->pdata, datalen);
    }
#endif

    DL_STR("checksum:%02x,%02x", pkg->checksum, lrc);
    lrc ^= pkg->checksum;


    if(lrc != 0) return -RET_ERR_INVAL;
  
    return RET_OK;
}

int s_CmdProcess()
{
    u16 cmd = 0;
    int ret = 0;
    u8 *databuf, result[4 + 8096];
    DATA_PACKAGE r_pkg, s_pkg;

    int amt_len, tmplen;
    char amount[33];
    int update_flag = 0;
    int update_ret;

    r_pkg.pdata = NULL;
    ret = s_PkgRecv(s_dl_com, &r_pkg, DL_TIME_OUT);  
    //DL_STR("s_PkgRecv ret %d", ret);
    if(ret) {
        if (r_pkg.pdata != NULL) {
            Sys_Free(r_pkg.pdata);
        }
        return -1;
    }

    cmd = (r_pkg.type<<8) | r_pkg.opscode;
    databuf = r_pkg.pdata;

    s_pkg.head = r_pkg.head;
    s_pkg.seq = r_pkg.seq;
    s_pkg.type = r_pkg.type;
    s_pkg.opscode = r_pkg.opscode;
    s_pkg.pdata = result;

    ret = RET_OK;
    switch(cmd) {
        case SYS_OTA_UPDATE:
            tmplen = r_pkg.datalen[0];
            tmplen <<= 8;
            tmplen |= r_pkg.datalen[1];
            //DBG_STR("tmplen:%d", tmplen);
            update_ret = CMD_SysUpdateProc(r_pkg.pdata, tmplen); 
            if (update_ret == 1) {
                update_flag = 1;
                ret = 0;
            } else if (update_ret == 0) {
                ret = 0;
            } else {
                ret = -1;
            }

            s_pkg.datalen[0] = 0x00;
            s_pkg.datalen[1] = 0x04;
            memcpy(result, (u8*)&ret, 4);
        break;
        case SYS_ADS_UPDATE:
            tmplen = r_pkg.datalen[0];
            tmplen <<= 8;
            tmplen |= r_pkg.datalen[1];
            //DBG_STR("tmplen:%d", tmplen);
            amt_len = transmit_file(r_pkg.pdata, tmplen, result);
            s_pkg.datalen[0] = (amt_len >> 8) & 0xFF;
            s_pkg.datalen[1] = amt_len & 0xFF;

        break;

        default:
            s_pkg.datalen[0] = 0x00;
            s_pkg.datalen[1] = 0x04;
            ret = -RET_ERR_INVAL;
            memcpy(result, (u8*)&ret, 4);
        break;
    }
    if (r_pkg.pdata != NULL) {
        Sys_Free(r_pkg.pdata);
    }
    
    s_PkgSend(s_dl_com, &s_pkg, 100);

    if (update_flag == 1) {
        LvosSysDelayMs(3000);
        LvosSysPower(1);
    }

    return 0;
}


static int show_menu()
{
    int idx;
    ST_MENUITEM menu[] = {
        {KB1, "1.entry app", NULL, (void *)NULL},
    };

    Ui_Clear();
    Ui_ClearKey();

    Ui_DispTitle("OTA SERVICE");

    for (idx = 0; idx < sizeof(menu)/sizeof(ST_MENUITEM);)
    {
        int line = (idx / MENU_1) + 1;
        Ui_DispText(0, line * Ui_GetCharHeight(), menu[idx].text, 0);
        idx++;
    }

    return 0;
}


void CmdProc(void)
{
    static int remain;
    u32 start_tick;
    int ret;
    int timeout = (10 * 1000);
    //int cnt;
    
    show_menu();
    

    start_tick = LvosSysTick();
    while (1) {
        
        ret = s_CmdProcess(); 
        if (ret < 0) {
            
        } else {
            start_tick = LvosSysTick();
        }

#if 1
        ret = Ui_GetKey();
        if (ret == KEY1) {
            break;
        }
#else
        cnt = LvosSysTick() - start_tick;
        if (cnt > timeout) {
            break;
        }

        ret = (timeout - cnt) / 1000;
        if (remain != ret) {
            remain = ret;
            //Ui_ClearLine(4, 4);
            Ui_Print(4, 0, "OTA service %d",  (timeout - cnt) / 1000);

            DBG_STR("ota service end  %d", (timeout - cnt) / 1000);
        }
#endif
    } 

    DBG_STR("ota service end");

    return;
}



