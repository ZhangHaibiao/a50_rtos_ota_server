#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "liteos_api.h"

#include "type.h"
#include "utils.h"
#include "app.h"
#include "log.h"

#include "litemapi.h"
#include "ui.h"


extern void CmdProc(void);
extern void lvos_mem_init(u32 buf[], u32 len);

void App_Main(void)
{
    int start;
    int ota_flag = 0;

     DBG_STR("app start");

    LiteMapiInit();

    start = LvosSysTick();
    while (1) { 
        if (Ui_GetKey() == KEY3) {
            ota_flag = 1;
            break;
        }

        if ((LvosSysTick() - start) > 500) {
            break;
        }
    }

    if (ota_flag) {
        PortOpen(USB_DEV, NULL);
        DBG_STR("m_UsbHandle:%d, m_IoHandle:%d", m_UsbHandle, m_IoHandle);
        
        //test_png();
        
        //download_file();
        
        CmdProc();
    }

    LvosAppExecute(NULL);

    while(1) {
        ;
    }

}


