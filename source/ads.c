
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "liteos_api.h"

#include "type.h"
#include "log.h"
#include "resource.h"

#define SOC_FLASH_ADDR          0x14000000
#define SOC_RAM_ADDR            0x06100000

#define FILE_NAME_ADS  "ads"
//#define FILE_NAME_ADS_UPDATE  "ads_update"

static int g_fd = -1;


static int save_data(u32 offset, u8 *data, u32 datalen, u32 totallen)
{
    u32 i;
    int ret;

    if (offset == 0) {
        if (g_fd >= 0) {
            LvosFclose(g_fd);
            g_fd = -1;
        }

        LvosFremove(FILE_NAME_ADS);

        DBG_STR("start write file, freesize %d", LvosFFreeSize());
        if (totallen > LvosFFreeSize()) {
            return -2;
        }

        g_fd = LvosFopen(FILE_NAME_ADS);
        if (g_fd < 0) {
            DBG_STR("open failed %d", g_fd);
            return -2;
        }
    }

    //DBG_HEX("recv", data, datalen);

    ret = LvosFwrite(g_fd, data, datalen);
    if (ret < 0) {
        DBG_STR("write file error fd %d,  %d", g_fd, ret);
        LvosFclose(g_fd);
        g_fd = -1;
        return -1;
    }
    
    if (((offset + datalen) == totallen)) {

    
        DBG_STR("write all");
        LvosFclose(g_fd);
        g_fd = -1;

#if 0
        u8 *tmp;
        int fd;
        u8 hash[32] = {0};

        tmp = (u8 *)malloc(totallen);
        DBG_STR("malloc %x, %d", tmp, totallen);
        fd = FsOpen(FILE_NAME_ADS, O_RDWR);
        DBG_STR("fd %d", fd);
        if (fd >= 0) {
            ret = FsRead(fd, tmp, totallen);
            DBG_STR("FsRead %d", ret);
            FsClose(fd);

            DBG_HEX("file", tmp, 256);

            LvosSha(2, hash, totallen, tmp);
            DBG_STR("hash cal");
            DBG_HEX("update", hash, sizeof(hash));
        }

        free(tmp);
#endif
    }

    return 0;
}

int update_ads(u8 *data, u32 len)
{
    u32 datalen;
    u32 offset;
    u32 totallen;
    int ret;

    offset    = ((u32)data[0]<<24) + ((u32)data[1]<<16) + ((u32)data[2]<<8 ) + ((u32)data[3] );
    datalen   = ((u32)data[4]<<24) + ((u32)data[5]<<16) + ((u32)data[6]<<8 ) + ((u32)data[7] );
    totallen  = ((u32)data[8]<<24) + ((u32)data[9]<<16) + ((u32)data[10]<<8) + ((u32)data[11]);

    if (len != (datalen + 12)) {
        return -1;
    }

    ret = save_data(offset, &data[12], datalen, totallen);
    if (ret < 0) {
        return -1;
    }

    return 0;
}


/*
*  
*
*
**/

int transmit_file(u8 *data, u32 len, u8 *rsp)
{
    u32 datalen;
    int ret;
    char name[256];
    u32 offset;
    u32 totallen;
    int op;
    int fd;
    u8 *content;
    FAT_ITEM file_info[260];


    offset    = ((u32)data[0]<<24) + ((u32)data[1]<<16) + ((u32)data[2]<<8 ) + ((u32)data[3] );
    datalen   = ((u32)data[4]<<24) + ((u32)data[5]<<16) + ((u32)data[6]<<8 ) + ((u32)data[7] );
    totallen  = ((u32)data[8]<<24) + ((u32)data[9]<<16) + ((u32)data[10]<<8) + ((u32)data[11]);
    content = &data[12];
    
    op = offset;
    if (op == 1) {
        memcpy(name, content, datalen);
        name[datalen] = '\0';

        DBG_STR("file name %s, mode %d ", name, totallen);

        if (totallen == 0) {
            LvosFremove(name);
            DBG_STR("remove file %s, fd %d", name, fd);
        }
        
        fd = LvosFopen(name);
        DBG_STR("open file %s, fd %d", name, fd);
        memcpy(rsp, &fd, sizeof(fd));
        return sizeof(fd);
    } else if (op == 2) {
        fd = totallen;
        ret = LvosFwrite(fd, content, datalen);
        DBG_STR("write file fd %d, len %d, ret %d", fd, datalen, ret);
        memcpy(rsp, &ret, sizeof(ret));
        return sizeof(ret);
    } else if (op == 3) {
        fd   = totallen;
        ret = LvosFclose(fd);
        DBG_STR("close file fd %d ", fd);
        memcpy(rsp, &ret, sizeof(ret));
        return sizeof(ret);
    } else if (op == 4) {
        fd = totallen;

        if (datalen > 241) {
            datalen = 241;
        }
        
        ret = LvosFread(fd, rsp + sizeof(ret), datalen);
        DBG_STR("read file fd %d, len %d, ret %d", fd, datalen, ret);
        
        memcpy(rsp, &ret, sizeof(ret));
        if (ret > 0) {
            return sizeof(ret) + ret;
        } else {
            return sizeof(ret);
        }
    } else if (op == 5) {
        //list
        int send_len;
        ret = LvosFFileinfo(file_info, sizeof(file_info) / sizeof(file_info[0]));
        DBG_STR("LvosFFileinfo %d", ret);
        memcpy(rsp, &ret, sizeof(ret));
        if (ret > 0) {
            DBG_STR("datalen %d", datalen);
            if (datalen > ret * sizeof(FAT_ITEM)) {
                datalen = ret * sizeof(FAT_ITEM);
            }

            if ((ret * sizeof(FAT_ITEM) - datalen) > 241) {
                send_len = 241;
            } else {
                send_len = ret * sizeof(FAT_ITEM) - datalen;
            }
            memcpy(rsp + sizeof(ret), ((u8 *)file_info) + datalen, send_len);
            return sizeof(ret) + send_len;
        } else {
            return sizeof(ret);
        }

    } else if (op == 6) {
        //remove
        memcpy(name, content, datalen);
        name[datalen] = '\0';

        DBG_STR("file name %s", name);
        
        ret = LvosFremove(name);
        DBG_STR("remove file %s, ret %d", name, ret);
        memcpy(rsp, &ret, sizeof(ret));
        return sizeof(ret);
    } else if (op == 7) {
        //free size
        char v[32] = {0};
        ret = LvosFFreeSize();
        DBG_STR("LvosFFreeSize %d", ret);
        memcpy(rsp, &ret, sizeof(ret));
        
        LvosSysCfgRead("FS_SPACE", v, sizeof(v));
        ret = atoi(v);
        DBG_STR("total Size %d", ret);
        memcpy(rsp + sizeof(ret), &ret, sizeof(ret));
        return sizeof(ret) * 2;
    } else {
        ret = -1;
        memcpy(rsp, &ret, sizeof(ret));
        return sizeof(ret);
    }

}

