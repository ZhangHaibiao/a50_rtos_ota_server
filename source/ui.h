#ifndef __UI_H__
#define __UI_H__


#define UI_FONT_NORMAL        1

#define UI_ATTR_FONTSIZE      0        // 0-小 1-普通 2-大
#define UI_ATTR_FONTCOLOR     1        // 字体颜色
#define UI_ATTR_BGCOLOR       2        // 背景颜色
#define UI_ATTR_REVER         3        // 1-带背景


#define UI_REDLIGHT           0x01
#define UI_GREENLIGHT         0x02
#define UI_YELLOWLIGHT        0x04
#define UI_BLUELIGHT          0x08
#define UI_ALLLIGHT           0X0F

#ifndef RGB
//0xFFRRGGBB
#define RGB(r, g, b)        (((u32)(((u8)(b) | ((u32)((u8)(g)) << 8)) | (((u32)(u8)(r)) << 16))))
#endif

/**0x00RRGGBB -> R5|G6|B5 16位色**/
#define RGB24TO16(val) ((u32)(((val>>16)&0xff) >> 3 << 11) | (u32)(((val>>8)&0xff) >> 2 << 5) | (u32)(((val&0xff)) >> 3) )
#define RGB_H(rgb) ((u8)((rgb)>>8))
#define RGB_L(rgb) ((u8)(rgb))

#define UI_RET_BASE          -1000
#define UI_RET_SUCCESS       0
#define UI_RET_FAIL          (UI_RET_BASE)
#define UI_RET_TIMEOUT       (UI_RET_BASE - 1)
#define UI_RET_ABORT         (UI_RET_BASE - 2)
#define UI_RET_PARAM         (UI_RET_BASE - 3)


#define DISPLAY_WIDTH       320
#define DISPLAY_HEIGHT      240
#define DISPLAY_LINE_SIZE   30

#define DISPLAY_CHAR_WIDTH   12  // px
#define DISPLAY_CHAR_HEIGHT  24  // px
#define DISPLAY_LINE_PADDING 4   // 320 不被24整除，多8个像素
//20(16x),26(12x),40(8x)
#define DISPLAY_LINE_NUM    10  // 240/24 = 10
#define DISPLAY_LINE_EN_NUM 26  // 320/12 = 26

#define DISPLAY_COLOR_BLACK  RGB(0, 0, 0)        // #000000
#define DISPLAY_COLOR_WHITE  RGB(255, 255, 255)  // #FFFFFF
#define DISPLAY_COLOR_GRAY   RGB(221, 221, 221)  // #DDDDDD
#define DISPLAY_COLOR_RED    RGB(138, 8, 8)      // #8A0808
#define DISPALY_COLOR_GREEN  RGB(11, 97, 11)     // #0B610B

#define DISPLAY_FONT_COLOR   DISPLAY_COLOR_BLACK
#define DISPLAY_BG_COLOR     DISPLAY_COLOR_WHITE


#define INPUT_MAX_SIZE       100

#define KB0            '0'
#define KB1            '1'
#define KB2            '2'
#define KB3            '3'
#define KB4            '4'
#define KB5            '5'
#define KB6            '6'
#define KB7            '7'
#define KB8            '8'
#define KB9            '9'
#define KB_DOT      '.'
#define KB_ENTER    '\r'
#define KB_CANCEL   0x1B
#define KB_NONE     0x00
#define KB_FN       KB_DOWN//0x01
#define KB_MENU        0x02
#define KB_CLEAR    0x03
#define KB_UP       0x05//KB_FN
#define KB_DOWN     0x06//KB_ENTER

#define KB_L1_CANCEL    KB_CANCEL
#define KB_L1_ENTER        KB_ENTER
#define KB_L1_UP           KB_UP//KB1
#define KB_L1_DOWN         KB_FN//KB2


typedef enum {
  DISPLAY_CENTER = 1,
  DISPLAY_LEFT,
  DISPLAY_RIGHT
}E_DISPALIGN;

typedef enum {
  MENU_1 = 1,
  MENU_2,
  MENU_3,
  MENU_4,
  MENU_5,
  MENU_6,
  MENU_7,
  MENU_8,
}E_MENUTYPE;


typedef int (*MenuCallback)(void * pxParam);

typedef struct {
  int          kb;
  const char * text;
  MenuCallback func;
  void *       param;
}ST_MENUITEM;

void Ui_ClearColor(u32 color);
void Ui_Clear();
void Ui_ClearBlockColor(int x, int y, int width, int height, u32 color);
void Ui_ClearBlock(int x, int y, int width, int height);
void Ui_ClearLineColor(int line, u32 color);
void Ui_ClearLine(int start, int end);
void Ui_DispTextColor(int x, int y, const char *text, u32 color);
void Ui_DispText(int x, int y, const char *text, int isReverse);
void Ui_DispTextAlign(int y, int align, const char *text, int isReverse);
void Ui_DispTextLineAlign(int line, int align, const char *text, int isReverse);
int Ui_GetLineNum();

void Ui_DispTitle(const char * title);

void Ui_ClearKey();
u8 Ui_GetKey();
u8 Ui_WaitKey(u32 timeMS);

int Ui_Menu(const char *title, const ST_MENUITEM *menu, int num, int mode, u32 timeMS);
int Ui_Select(const char *title, const char *info, const ST_MENUITEM * menu, int num, int mode, u32 timeMS);


int Ui_InputStr(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS);
int Ui_InputNum(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS);
int Ui_InputPwd(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS);
int Ui_InputHex(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS);
int Ui_InputIp(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS);


void Ui_Print(u32 row, u32 mode, const char *str, ...);
void ScrPrnStr(u32 row, const char *str, ...);

void Utils_BeepFAIL();
void Utils_BeepOK();
#endif

