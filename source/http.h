#ifndef __HTTP_H__
#define __HTTP_H__

#define HTTP_BUFFER_SIZE 4096


#define  APP_UI_TIMEOUT (15 * 1000)

typedef struct
{
	int socket;
	int status;
	int isTrunked;
	int dataLength;
	int headerLength;
	int trunckedDataLen;  //整个数据内容
	int packLen;
	int packSize;
	char *pack;
} HTTPHANDLE;

typedef enum{
  HTTP_ERR_PARAM = -500,
  HTTP_ERR_USERCANCEL,
  HTTP_ERR_TIMEOUT,
  HTTP_ERR_CONNECT,
  HTTP_ERR_RECV,
  HTTP_ERR_SEND,
  HTTP_ERR_DATA,
  HTTP_ERR_NEED_RECV, // 需要继续接收
  HTTP_ERR_STATUS,
  HTTP_ERR_LEGNTH,
  HTTP_ERR_SIZE,
  HTTP_SUCCESS = 0
}HTTPRETURN;

typedef int (*DecodeCB)(void * data, int len, void * param);

int httpStatus;

int Http_InitHandle(HTTPHANDLE * handle, uint size);

void Http_Release(HTTPHANDLE * handle);

int Http_SetHeader(HTTPHANDLE * request, const char * key, const char * value);

int Http_InitRequest(HTTPHANDLE * request, const char * method, const char * host, const char * port, const char * url);

int Http_SetRequestBody(HTTPHANDLE * request, const char * body, uint len);

int Http_GetResponseCode(HTTPHANDLE *handle);

int Http_GetResponseHeader(HTTPHANDLE * handle, char * data, int dataSize);

int Http_GetResponseBody(HTTPHANDLE * handle, char * data, int dataSize);

int Http_Connect(HTTPHANDLE * handle, const char * host, const char * port, int timeout);

int Http_Send(HTTPHANDLE * handle);

int Http_Recv(HTTPHANDLE * handle, uint timeMS);

int Http_Close(HTTPHANDLE * handle);


int Http_DecodeResponseBody(HTTPHANDLE * response, void * param, DecodeCB callback);

int Http_Post(char *uri, byte *hostIp, byte *hostPort, byte *sendData, byte *recvData, int sizeofRecvData);

void Http_SetShowPrompt(byte isShow);

#endif

