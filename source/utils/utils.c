#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#include "liteos_api.h"
#include "type.h"



int Sys_GetHandle(const char * pdev_name)
{   
    char keyword[64];   
    char attr[64];      
    int  iret;      
    int  i;     
    
    for(i = 0;; i++)    
    {
        iret = LvosParamEnum(1000, i, keyword, attr, sizeof(attr));         
        if(iret){
			break;
        }
		
        if(strcmp(pdev_name, keyword)){
			continue;
        }
		
        return atoi(attr);
    }
	
    return -1;
}

int Sys_ReadParam(int handle, const char * name, char * val, uint valSize)
{
	return LvosParamRead(handle, (char *)name, (byte *)val, valSize);
}

int Sys_WriteParam(int handle, const char * name, const char * val)
{
	return LvosParamWrite(handle, (char *)name, (byte *)val);
}

int Sys_ReadCfg(const char * keyword, byte * val, uint size)
{
 	int iRet;
	
	memset(val, 0, size);
 	iRet = LvosSysCfgRead((char *)keyword, val, size);
	if(iRet < 0) {
		SYS_STRLOG("LvosSysCfgRead(%s) = %d", keyword, iRet);
		return -1;
	}
	iRet = strlen(val);
	SYS_STRLOG("%s = %s", keyword, val);
	
	return iRet;
}

