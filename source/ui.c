#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "liteos_api.h"
#include "LiteMapi.h"
#include "ui.h"

typedef enum {
    INPUT_NUM = 1,
    INPUT_STR,
    INPUT_AMT,
    INPUT_HEX,
    INPUT_IP,
    INPUT_PWD
}E_INPUTTYPE;

static const char *m_keyMap[10] =
{
    "0,.*# ~`!@$%^&-+=(){}[]<>_|\\:;\"\'?/",
    "1 QZqz", 
    "2ABCabc", 
    "3DEFdef",
    "4GHIghi", 
    "5JKLjkl", 
    "6MNOmno",
    "7PRSprs", 
    "8TUVtuv", 
    "9WXYwxy",
};


static u8 m_StatusBar = 0;


int Ui_GetLcdWidth()
{
    return DISPLAY_WIDTH;
}

int Ui_GetLcdHeight()
{
    return DISPLAY_HEIGHT;
}

int Ui_GetCharWidth()
{
    return DISPLAY_CHAR_WIDTH;
}

int Ui_GetCharHeight()
{
    return DISPLAY_CHAR_HEIGHT;
}

int Ui_GetLineEnNum()
{
    return DISPLAY_LINE_EN_NUM;
}

int Ui_GetLinePadding()
{
    return DISPLAY_LINE_PADDING;
}

int Ui_GetLineNum()
{
    return DISPLAY_LINE_NUM;
}

void Ui_ClearBlockColor(int x, int y, int width, int height, u32 color)
{
    u32 datalen, i;
    u8 *data = NULL;
    u16 rgb;

    datalen = width*height*2;
    data = malloc(datalen);
    if(data == NULL) return -2;

    rgb = RGB24TO16(color);
//DBG_STR("x:%d,y:%d, width:%d, height:%d, color:%08x, rgb:%04x", x,y, width, height, color, rgb);
    for(i=0; i<(datalen/2); i++){
        data[2*i] = RGB_L(rgb);
        data[2*i+1] = RGB_H(rgb);
    }

//DBG_HEX("DATA:", data, 128);
    CLcdDrawArea(x, y, width, height, data);

    free(data);
}

void Ui_ClearBlock(int x, int y, int width, int height)
{
    Ui_ClearBlockColor(x, y, width, height, DISPLAY_BG_COLOR);
}

void Ui_ClearColor(u32 color)
{
    Ui_ClearBlockColor(0, 0, Ui_GetLcdWidth(), Ui_GetLcdHeight(), color);
}

void Ui_Clear()
{
    Ui_ClearColor(DISPLAY_BG_COLOR);
}

void Ui_ClearLineColor(int line, u32 color)
{
    Ui_ClearBlockColor(0, line * Ui_GetCharHeight(), Ui_GetLcdWidth(), Ui_GetCharHeight(), color);
}

void Ui_ClearLine(int start, int end)
{
    Ui_ClearBlock(0, start*Ui_GetCharHeight(), Ui_GetLcdWidth(), (end-start+1)*Ui_GetCharHeight());
}

void Ui_Disp(int x, int y, const char *text, u32 fontColor, u32 bgColor, u32 size, u32 rever)
{
    CLcdSetFgColor(fontColor);
    CLcdSetBgColor(bgColor);   
    Ui_ClearBlockColor(x, y, Ui_GetLcdWidth(), Ui_GetCharHeight(), bgColor);
    CLcdTextOut(x,y, text); 
}

void Ui_DispTextColor(int x, int y, const char *text, u32 color)
{
    Ui_Disp(x, y, text, color, DISPLAY_BG_COLOR, UI_FONT_NORMAL, 0);
}

void Ui_DispText(int x, int y, const char *text, int isReverse)
{
    if(isReverse) {
        Ui_Disp(x, y, text, DISPLAY_BG_COLOR, DISPLAY_FONT_COLOR, UI_FONT_NORMAL, 1);
    } else {
        Ui_Disp(x, y, text, DISPLAY_FONT_COLOR, DISPLAY_BG_COLOR, UI_FONT_NORMAL, 0);
    }
}
int Utils_StrCopy(char * dst, const char *src, int len)
{
	char *p = dst;
	int icNum = 0, i = 0, ic = 0, pos = 0;

	memcpy(p, src, len);
	p[len] = 0x00;
	ic = strlen(p);

	if (ic>0){
		for(i=0; i<ic; i++){
			if ( (unsigned char)(*(p+i))==0x00 ) { break;   }
			if ( (unsigned char)(*(p+i))>0xA0 )  { icNum++; pos = i; }
		}
		if ( icNum%2==1 ) { *(p+pos) = 0x00; ic = pos; } 
	}

	return (ic);

}
void Ui_DispTextAlign(int y, int align, const char *text, int isReverse)
{
    char szText[DISPLAY_LINE_SIZE + 1];

    int iLen = 0;
    int x = 0;

    memset(szText, 0, sizeof(szText));
    iLen = Utils_StrCopy(szText, text,  Ui_GetLineEnNum());
    if (iLen < 0)
    {
        return;
    }

    if (align == DISPLAY_CENTER)
    {
        x = ((Ui_GetLineEnNum() - iLen) * Ui_GetCharWidth()) / 2;
    }
    else if (align == DISPLAY_RIGHT)
    {
        x = ((Ui_GetLineEnNum() - iLen) * Ui_GetCharWidth());
    }

    x += Ui_GetLinePadding();
    
    Ui_DispText(x, y, szText, isReverse);
}

void Ui_DispTextLineAlign(int line, int align, const char *text, int isReverse)
{
    Ui_DispTextAlign(line * Ui_GetCharHeight(), align, text, isReverse);
}


void Ui_DispTitle(const char * title)
{
    Ui_ClearLineColor(0, DISPLAY_FONT_COLOR);
    Ui_DispTextLineAlign(0, DISPLAY_CENTER, title, 1);
}

void Ui_ClearKey()
{
    kbflush();
}

u8 Ui_GetKey()
{
    if(kbhit()) return KB_NONE;
    return getkey();
}

u8 Ui_WaitKey(u32 timeMS)
{
    u8 kb;
    u32 start = GetTimerCount();
    
    if (!timeMS) {
        return Ui_GetKey();
    }
    
    while (timeMS > (GetTimerCount() - start))
    {
        if(kbhit() != 0) {
            continue;
        }
        return getkey();
    }
    
    return KB_NONE;
}
int Ui_Menu(const char *title, const ST_MENUITEM * menu, int num, int mode, u32 timeMS)
{
    int idx;
    int item;
    int iRet;
    int isTitle;
    int iWaitForEver;

    iWaitForEver = (0xffffffff == timeMS);
    if(iWaitForEver)
    {
        timeMS = 500;
    }

    if (menu == NULL || num > 9 || num <= 0)
    {
        return UI_RET_PARAM;
    }

MENU_DRAW_MENU:

    if (m_StatusBar == 1)
    {
        isTitle = 1;
    }
    else
    {
        isTitle = 0;
    }

    Ui_Clear();
    Ui_ClearKey();
    if (title != NULL)
    {
        Ui_DispTitle(title);
        isTitle += 1;
    }
    for (idx = 0; idx < num;)
    {
        int line = (idx / mode) + isTitle;
        for (item = 0; item < mode && idx < num; item++)
        {
            Ui_DispText((Ui_GetLcdWidth() * item) / mode, line * Ui_GetCharHeight(), menu[idx++].text, 0);
        }
    }
MENU_WAIT_KEY:

    //s_SysIconProc();
    ///s_PowerProc();
    
    iRet = Ui_WaitKey(timeMS);
    if (iRet == KB_NONE)
    {
        if(iWaitForEver)
        {
            goto MENU_WAIT_KEY;
        }
        // return UI_RET_TIMEOUT;
        return 0;
    }
    if (iRet == KB_CANCEL)
    {
        // return UI_RET_ABORT;
        return 0;
    }

    for (idx = 0; idx < num; idx++)
    {
        if (menu[idx].kb != iRet)
        {
            continue;
        }
        if (menu[idx].func != NULL)
        {
            iRet = menu[idx].func(menu[idx].param);
            if (iRet)
            { // exit the menu
                return iRet;
            }
            goto MENU_DRAW_MENU;
        }
        goto MENU_WAIT_KEY;
    }

    goto MENU_WAIT_KEY;
}


int Ui_Select(const char *title, const char *info, const ST_MENUITEM * menu, int num, int mode, u32 timeMS)
{
    int idx;
    int item;
    int iRet;
    int isTitle;

    if (menu == NULL || num > (9*mode) || num <= 0)
    {
        return UI_RET_PARAM;
    }

MENU_DRAW_MENU:

    if (m_StatusBar == 1)
    {
        isTitle = 1;
    }
    else
    {
        isTitle = 0;
    }

    Ui_ClearKey();
    if (title != NULL)
    {
        Ui_DispTitle(title);
    } 
    isTitle += 1;
    Ui_ClearLine(1, Ui_GetLineNum()-1);
    if (info != NULL)
    {
        Ui_DispTextLineAlign(1, DISPLAY_LEFT, info, 0);
        isTitle += 1;
    }
    
    for (idx = 0; idx < num;)
    {
        int line = (idx / mode) + isTitle;
        for (item = 0; item < mode && idx < num; item++)
        {
            Ui_DispText((Ui_GetLcdWidth() * item) / mode, line * Ui_GetCharHeight(), menu[idx++].text, 0);
        }
    }
    

MENU_WAIT_KEY:

    iRet = Ui_WaitKey(timeMS);
    if (iRet == KB_NONE)
    {
        return UI_RET_TIMEOUT;
    }
    if (iRet == KB_CANCEL)
    {
        return UI_RET_ABORT;
    }

    for (idx = 0; idx < num; idx++)
    {
        if (menu[idx].kb != iRet)
        {
            continue;
        }
        return (int) menu[idx].param;
    }

    goto MENU_WAIT_KEY;
}

static char Ui_GetKeyMask(char curKey, u32 *times)
{
    u8 i;
    
    if(curKey == 0) {
        return 0;
    }

    for(i = 0; i < 10; i++) {
        u8 idx = 0;
        char * keyptr = (char *)m_keyMap[i];
    
        while(keyptr[idx] != 0) {
            if(keyptr[idx] == curKey) {
                *times = idx;
                return (char)(i+KB0);
            }
            idx++;
        }
    }
    
    return 0;
}

static char Ui_GetRealKey(char curKey, u32 times, int mode)
{
    char * keyptr;

    if(curKey < KB0 || curKey > KB9) {
        return curKey;
    }
    
    keyptr = (char *)m_keyMap[curKey-KB0];
    
    switch(mode)
    {
    case INPUT_PWD:
    case INPUT_NUM:
        return curKey;
    
    case INPUT_STR:
        times %= strlen(keyptr);
        return keyptr[times];
                
    case INPUT_HEX:
        if(curKey != KB2 && curKey != KB3) {
            return curKey;
        }
        times %= 4;
        return keyptr[times];
        
    case INPUT_IP:
        if(curKey != KB1) {
            return curKey;
        } 
        times %= 2;
        if(times) {
            return '.';
        }
        return curKey;
    }
    
    return curKey;
}

int Ui_Input(int start, const char *title, const char *text, char *value, int mode, u32 max, u32 min, u32 timeMS)
{
    char data[INPUT_MAX_SIZE + 1];
    int line;
    char curKey;
    u32 times;

    if (text == NULL || value == NULL || max == 0 || max > INPUT_MAX_SIZE || strlen(value) > max)
    {
        return UI_RET_PARAM;
    }

    Ui_ClearKey();
    line = start;
    if (title != NULL && strlen(title))
    {
        Ui_DispTitle(title);
    }
    Ui_ClearLine(start, Ui_GetLineNum()-1);
    
    if (text != NULL)
    {
        if(strlen(text)){
            Ui_DispTextLineAlign(line++, DISPLAY_LEFT, text, 0);
        }
    }

    // deal default value
    memset(data, 0, sizeof(data));
    strcpy(data, value);
    if(strlen(data)) {
        times = 0;
        curKey = Ui_GetKeyMask(data[strlen(data) - 1], &times);
    } else {
        curKey = KB_NONE;
    }
    
    while (1)
    {
        u8 kbVal = 0;
        char disp[INPUT_MAX_SIZE + 2];
        int len = strlen(data);
        
        // format to display
        memset(disp, 0, sizeof(disp));
        if (len > Ui_GetLineEnNum())
        {
            if(mode == INPUT_PWD) {
                memset(disp, '*', Ui_GetLineEnNum());
                disp[Ui_GetLineEnNum()] = 0;
            } else {
                strcpy(disp, data + len - Ui_GetLineEnNum());
            }
        }
        else
        {
            if(mode == INPUT_PWD) {
                memset(disp, '*', len);
                disp[len] = 0;
            } else {
                   strcpy(disp, data);
            }
        }
        
        Ui_ClearLine(line, line);
        Ui_DispTextLineAlign(line, DISPLAY_RIGHT, disp, 0);
        
        // wait input
        kbVal = Ui_WaitKey(timeMS);
        if (kbVal == KB_NONE)
        {
            return UI_RET_TIMEOUT;
        }
        if(kbVal == KB_FN && curKey != KB_NONE) {
            if(mode == INPUT_STR || mode == INPUT_HEX || mode == INPUT_IP) {
                kbVal = Ui_GetRealKey(curKey, times, mode);
                data[len-1] = kbVal;
                times ++;
            }
            continue;
        }
        
        if(kbVal >= KB0 && kbVal <= KB9) {
            curKey = kbVal;
            times = 0;
            if (len < max) {
                data[len] = curKey;
            } else {
                data[len-1] = curKey;
            }
            continue;
        }
        
        switch (kbVal)
        {
        case KB_DOWN:
            if(mode == INPUT_IP) {
                curKey = kbVal;
                times = 0;
                if (len < max) {
                    data[len] = '.';
                } else {
                    data[len-1] = '.';
                }
            }
            break;
            
        case KB_CLEAR:
            if (len > 0) {
                data[len-1] = 0;
                times = 0;
                curKey = Ui_GetKeyMask(data[len-1], &times);
            }
            break;

        case KB_ENTER:
            if (len >= min)
            {
                strcpy(value, data);
                return UI_RET_SUCCESS;
            }
            break;

        case KB_CANCEL:
            return UI_RET_ABORT;

        default:
            break;
        }
    }
}

int Ui_InputNum(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS)
{
    return Ui_Input(1, title, text,  value, INPUT_NUM, max, min, timeMS);
}

int Ui_InputPwd(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS)
{
    return Ui_Input(1, title, text,  value, INPUT_PWD, max, min, timeMS);
}

int Ui_InputStr(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS)
{
    return Ui_Input(1, title, text,  value, INPUT_STR, max, min, timeMS);
}


int Ui_InputHex(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS)
{
    return Ui_Input(1, title, text,  value, INPUT_HEX, max, min, timeMS);
}

int Ui_InputIp(const char * title, const char * text, char * value, u32 max, u32 min, u32 timeMS)
{
    return Ui_Input(1, title, text,  value, INPUT_IP, max, min, timeMS);
}

void Ui_Print(u32 row, u32 mode, const char *str, ...)
{
    int iWidth, iHeight;
    va_list varg;
    int retv;
    char buffer[64];

    va_start(varg, str);
    retv = vsnprintf(buffer, sizeof(buffer) - 4, str, varg);
    va_end(varg);
    buffer[retv] = 0;

    Ui_DispTextLineAlign(row, DISPLAY_LEFT, buffer, mode);
}

void ScrPrnHEX(u32 row, u8 *data, u32 len)
{
    int retv, col_per_byte, byte_per_row, row_n, i, j, cnt;
    u8 buf[32];

    col_per_byte = 12*3;//"xx "
    byte_per_row = 320/col_per_byte;
    row_n = (len+byte_per_row-1)/byte_per_row;

    cnt=0;
    for(j=0; j<(row_n*2); j+=2){
        for(i=0; i<byte_per_row; ){
            memset(buf, 0x00, sizeof(buf));
            sprintf(buf, "%02x", data[i+byte_per_row*j/2]);
            Ui_DispText(i*col_per_byte, (row+j) *12,buf, 0);
            i++;
            cnt++;
            if(cnt>=len) return;
        }    
    }
}

void ScrPrnStr(u32 row, const char *str, ...)
{
     va_list varg;
    int retv, len, col_per_ch, ch_per_row, row_n, i, j, cnt;
    char buffer[256];
    u8 buf[32];

    va_start(varg, str);
    retv = vsnprintf(buffer, sizeof(buffer) - 4, str, varg);
    va_end(varg);
    buffer[retv] = 0;

    len = retv;//datalen=64, 
    col_per_ch = 12;
    ch_per_row = 320/col_per_ch;
    row_n = (len+ch_per_row-1)/ch_per_row;

    cnt=0;
    for(j=0; j<(row_n*2); j+=2){
        for(i=0; i<ch_per_row; ){
            memset(buf, 0x00, sizeof(buf));
            sprintf(buf, "%c", buffer[i+ch_per_row*j/2]);
            Ui_DispText(i*col_per_ch, (row+j)*12,buf, 0);
            i++;
            cnt++;
            if(cnt>=len) return;
        }    
    }
}

int s_LcdTest()
{
    int i=0,j, r,g,b;
    int x,y;

    j=0;
    i=0;

    while(1){

        Ui_ClearBlockColor(0, 0, 320, 240, RGB(0xff,0,0));//RED
        //if(Ui_WaitKey(3000)==KB_ENTER) break;
        if(getkey()==KB_ENTER) break;

        Ui_ClearBlockColor(0, 0, 320, 240, RGB(0,0xff,0));//GREEN
        //if(Ui_WaitKey(3000)==KB_ENTER) break;
        if(getkey()==KB_ENTER) break;

        Ui_ClearBlockColor(0, 0, 320, 240, RGB(0,0,0xff));//BLUE
        //if(Ui_WaitKey(3000)==KB_ENTER) break;
        if(getkey()==KB_ENTER) break;

        Ui_Clear();
        //if(Ui_WaitKey(3000)==KB_ENTER) break;
        if(getkey()==KB_ENTER) break;
    }

	return 0;
}